package configuracoes;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "classpath:features", 
		glue = {"classpath:steps", "hooks"}, 
		
		monochrome = true, 
		dryRun = false, 
		strict = false,
		tags = {"@Login" })
public class RunnerAutomation {

}
