package configuracoes;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class AppiumService {

	//Build the Appium service
	AppiumServiceBuilder builder = new AppiumServiceBuilder();
	
	//Start the server with the builder
    AppiumDriverLocalService service = AppiumDriverLocalService.buildService(builder);
	
	public void startServer() {
	    
	    builder.withIPAddress("0.0.0.0");
	    builder.usingPort(4724);
	    builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
	    builder.withArgument(GeneralServerFlag.LOG_LEVEL,"error");

	    service.start();
	}

	public void stopServer() {
	    service.stop();
	}
}
